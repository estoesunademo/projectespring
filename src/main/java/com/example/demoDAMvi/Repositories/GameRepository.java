package com.example.demoDAMvi.Repositories;

import com.example.demoDAMvi.Models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, Integer>
{
	Game findByIdGame(int id);
}
