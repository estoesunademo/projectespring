package com.example.demoDAMvi.Services;

import com.example.demoDAMvi.Models.Maker;
import com.example.demoDAMvi.Models.Typemaker;

import javax.lang.model.element.TypeElement;
import java.util.List;

public interface IMakerService {
    List<Maker> findByName(String nom);
    Maker findByIdMaker(int id);
    List<Maker> getMakersByTypeWithoutCorporation(Typemaker typemaker);
    List<String> getMakersByTypeWithCorporation(Typemaker typemaker, int idCorporation);

}
