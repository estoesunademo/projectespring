package com.example.demoDAMvi.Services;

import com.example.demoDAMvi.Models.Game;
import com.example.demoDAMvi.Models.Resultat;

import java.util.List;

public interface IGameService {

    Resultat isEndedGame();

    Resultat setWinnerGame();

}
