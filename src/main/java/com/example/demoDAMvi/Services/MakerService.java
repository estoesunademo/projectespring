package com.example.demoDAMvi.Services;

import com.example.demoDAMvi.Models.Maker;
import com.example.demoDAMvi.Models.Typemaker;
import com.example.demoDAMvi.Repositories.MakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("MakerService")
public class MakerService implements IMakerService {

    @Autowired
    MakerRepository mr;

    public List<Maker> findAll() {
        return mr.findAll();
    }

    @Override
    public List<Maker> findByName(String nom) {
        return mr.findByName(nom);
    }

    @Override
    public Maker findByIdMaker(int id) {
        return mr.findByIdMaker(id);
    }

    @Override
    public List<Maker> getMakersByTypeWithoutCorporation(Typemaker typemaker) {
        List<Maker> makers = mr.findAll();
        System.out.println(makers);
        List<Maker> res = new ArrayList<>();
        for (Maker m : makers) {
            if (m.getTypemaker() == typemaker && m.getCorporation() == null) res.add(m);
        }
        System.out.println(res);
        return res;
    }

    public List<String> getMakersByTypeWithoutCorporationString(Typemaker typemaker) {
        List<Maker> makers = mr.findAll();
        System.out.println(makers);
        List<String> res = new ArrayList<>();
        for (Maker m : makers) {
            if (m.getTypemaker() == typemaker && m.getCorporation() == null) res.add(m.toString());
        }
        System.out.println(res);
        return res;
    }

    @Override
    public List<String> getMakersByTypeWithCorporation(Typemaker typemaker, int idCorporation) {
        List<Maker> makers = mr.findAll();
        System.out.println(makers);
        List<String> res = new ArrayList<>();
        for (Maker m : makers) {
            if (m.getCorporation() != null && m.getCorporation().getIdCorporation() == idCorporation) res.add(m.toString());
        }
        System.out.println(res);
        return res;
    }
}
