package com.example.demoDAMvi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.example.demoDAMvi"})
@EntityScan("com.example.demoDAMvi")
@EnableJpaRepositories("com.example.demoDAMvi")
public class ProjecteSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjecteSpringApplication.class, args);
	}

}
