package com.example.demoDAMvi.Controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.example.demoDAMvi.Models.*;
import com.example.demoDAMvi.Services.DicesService;
import com.example.demoDAMvi.Services.GameService;
import com.example.demoDAMvi.Services.MakerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.example.demoDAMvi.Services.CorporationService;

@RestController
public class Controller {

    @Autowired
    CorporationService cs;

    @Autowired
    MakerService ms;

    @Autowired
    GameService gs;

    @Autowired
    DicesService ds;

    @GetMapping(path = "/hello")
    public String hello() {
        return "hola ";
    }

    @GetMapping(path = "/test")
    public String test(@RequestParam String a, @RequestParam String b) {
        return "hola " + a + " y ";
    }

    //localhost:8080/hello2param?nomAlumne=Manuel&nomAlumne2=Dario
    @GetMapping(path = "/hello2param")
    public String hello(@RequestParam String nomAlumne, @RequestParam String nomAlumne2) {
        return "hola " + nomAlumne + " y " + nomAlumne2;
    }

    @GetMapping(path = "/getMakersByTypeWithoutCorporation")
    public List<String> getMakersByTypeWithoutCorporationString(@RequestParam Typemaker typemaker) {
        return ms.getMakersByTypeWithoutCorporationString(typemaker);
    }

    @GetMapping(path = "/getCorporationsWithPlayer")
    public List<String> getCorporationsWithPlayer() {
        return cs.getCorporationsWithPlayer();
    }

    @GetMapping(path = "/getMakersByTypeWithCorporation")
    public List<String> getMakersByTypeWithCorporation(@RequestParam Typemaker typemaker, @RequestParam int idCorporation) {
        return ms.getMakersByTypeWithCorporation(typemaker, idCorporation);
    }

    @GetMapping(path = "/rollingDices")
    public Dices rollingDices() {
        Random r = new Random();
        List<Integer> tirades = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            tirades.add(r.nextInt(1, 7));
        }
        Dices d = new Dices(tirades, 69);
        return d;
    }

    @PostMapping(path = "/resolveDices")
    private Resultat resolveDices(@RequestBody Dices dices) {
        return ds.resolveDices(dices);
    }

    @GetMapping(path = "/isEndedGame")
    public Resultat isEndedGame() {
        return gs.isEndedGame();
    }

    @GetMapping(path = "/setVictoryPointsMakers")
    public List<String> setVictoryPointsMakers() {
        return cs.setVictoryPointsMakers();
    }

    @GetMapping(path = "/setWinnerGame")
    public Resultat setWinnerGame() {
        return gs.setWinnerGame();
    }
}
